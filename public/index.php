<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// collego ORM Redbean per collegare database 
\RedBeanPHP\R::setup('mysql:host=127.0.0.1; dbname=pw2', 'root', '');

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// P.S. Register controller
require __DIR__ . '/../src/Controller/ProductsController.php';
require __DIR__ . '/../src/Controller/OrdersController.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();