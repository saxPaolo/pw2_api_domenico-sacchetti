<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;

use \RedBeanPHP\R as DB;

/* 
class ProductsTableFormatter implements RedBean_IBeanFormatter{
    public function formatBeanTable($table) {
        return $table;
    }
    public function formatBeanID( $table ) {
        if ($table=="prodotti") return "prod_id";
        return "id";
    }
}

R::$writer->tableFormatter = new ProductsTableFormatter;
$products = R::find( "prodotti" ); 
*/

class ProductsController {

    protected $container;

    public function __construct(\Slim\Container $container) {
        $this->container = $container;
    }

    /**
     * Ritorna in json tutti i prodotti
     *
     * @return void
     */
    public function getAllProducts(Request $request, Response $response, array $args) {
        $products = DB::find('prodotti');

        return $response->withJson(DB::exportAll($products));
    }

    public function getProduct(Request $request, Response $response, array $args) {
        //Carico il prodotto da DB basandomi sull'id
        $product = DB::load('prodotti', $args['id']);
        //Se il prodotto non è stato trovato
        if(!$product->id) {
            //Ritorno un errore 404
            return $response->withStatus(404);
        } else {
            return $response->withJson(DB::exportAll($product));
        }
    }

    /**
     * Funzione di eliminazione da DB di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function deleteProduct(Request $request, Response $response, array $args) {
        //Carico il prodotto da DB basandomi sull'id
        $product = DB::load('prodotti', $args['id']);
        //Se il prodotto non è stata trovata
        if(!$product->id) {
            //Ritorno un errore 404
            return $response->withStatus(404);
        } else {
            //Elimino il prodotto dal db
            DB::trash($product);
            //Ritorno uno stato 204
            return $response->withStatus(204);
        }
    }

    /**
     * Funzione per la creazione di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function createProduct(Request $request, Response $response, array $args) {
        
        //Carico in $parametri i dati inviati via POST
        $parametri = $request->getParsedBody();

        $parametri = ($parametri==null) ? [] : $parametri;

        error_reporting(E_ALL & ~E_NOTICE);

        if(array_key_exists('prodname', $parametri)) {
            if(!$parametri['prodname']) {
                return $response->withStatus(400)
                ->withJson(
                    array(
                        'status'=> false, 
                        'message' => 'il nome è vuoto'
                    )
                );
            }
        } else {
            return $response->withStatus(400)
                ->withJson(
                    array(
                        'status'=> false, 
                        'message' => 'non hai inviato il nome via POST'
                    )
            );
        }
        $product = DB::dispense('prodotti');
        $product->prodname = $parametri['prodname'];
        $product->proddesc = $parametri['proddesc'];
        $product->prodprice = $parametri['prodprice'];
        $product->prodimg = $parametri['prodimg'];

        DB::store($product);
        return $response->withStatus(201);
    }

    /**
     * Update di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function updateProduct(Request $request, Response $response, array $args) {
        error_reporting(E_ALL & ~E_NOTICE);

        //Carico il prodotto da DB basandomi sull'id
        $product = DB::load('prodotti', $args['id']);
        if(!$product->id) {
            return $response->withStatus(404);
        }
        //Carico in $parametri i dati inviati via PUT
        $parametri = $request->getParsedBody();
        if($parametri['prodname']) {
            $product->prodname = $parametri['prodname'];
        }
        $product->proddesc = $parametri['proddesc'];
        $product->prodprice = $parametri['prodprice'];
        $product->prodimg = $parametri['prodimg'];

        DB::store($product);
    }
}