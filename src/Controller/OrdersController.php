<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;

use \RedBeanPHP\R as DB;

class OrdersController {

    protected $container;

    public function __construct(\Slim\Container $container) {
        $this->container = $container;
    }

    /**
     * Ritorna in json tutti i prodotti
     *
     * @return void
     */
    public function getAllOrders(Request $request, Response $response, array $args) {
        $orders = DB::find('ordini');

        //print_r($orders);

        return $response->withJson(DB::exportAll($orders));
    }

    public function getOrder(Request $request, Response $response, array $args) {
        //Carico il prodotto da DB basandomi sull'id
        $order = DB::load('ordini', $args['id']);
        //Se il prodotto non è stato trovato
        if(!$order->id) {
            //Ritorno un errore 404
            return $response->withStatus(404);
        } else {
            return $response->withJson(DB::exportAll($order));
        }
    }

    /**
     * Funzione di eliminazione da DB di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function deleteOrder(Request $request, Response $response, array $args) {
        //Carico il prodotto da DB basandomi sull'id
        $order = DB::load('ordini', $args['id']);
        //Se il prodotto non è stata trovata
        if(!$order->id) {
            //Ritorno un errore 404
            return $response->withStatus(404);
        } else {
            //Elimino il prodotto dal db
            DB::trash($order);
            //Ritorno uno stato 204
            return $response->withStatus(204);
        }
    }

    /**
     * Funzione per la creazione di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function createOrder(Request $request, Response $response, array $args) {
        
        //Carico in $parametri i dati inviati via POST
        $parametri = $request->getParsedBody();

        $parametri = ($parametri==null) ? [] : $parametri;

        error_reporting(E_ALL & ~E_NOTICE);

        if(array_key_exists('id', $parametri)) {
            if(!$parametri['id']) {
                return $response->withStatus(400)
                ->withJson(
                    array(
                        'status'=> false, 
                        'message' => 'l\'ID è vuoto'
                    )
                );
            }
        } else {
            return $response->withStatus(400)
                ->withJson(
                    array(
                        'status'=> false, 
                        'message' => 'non hai inviato l\'ID  via POST'
                    )
            );
        }
        $order = DB::dispense('ordini');
        $order->data = $parametri['orddata'];
        $order->indirizzo = $parametri['ordindirizzo'];
        $order->num = $parametri['ordstrnum'];
        $order->citta = $parametri['ordcitta'];
        $order->email = $parametri['ordemail'];
        $order->tel = $parametri['ordtel'];
        $order->orario = $parametri['ordorario'];

        DB::store($order);
        return $response->withStatus(201);
    }

    /**
     * Update di un prodotto
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function updateOrder(Request $request, Response $response, array $args) {
        error_reporting(E_ALL & ~E_NOTICE);

        //Carico il prodotto da DB basandomi sull'id
        $order = DB::load('ordini', $args['id']);
        if(!$order->id) {
            return $response->withStatus(404);
        }
        //Carico in $parametri i dati inviati via PUT
        $parametri = $request->getParsedBody();
        if($parametri['id']) {
            $order->id = $parametri['id'];
        }
        $order->data = $parametri['orddata'];
        $order->indirizzo = $parametri['ordindirizzo'];
        $order->num = $parametri['ordstrnum'];
        $order->citta = $parametri['ordcitta'];
        $order->email = $parametri['ordemail'];
        $order->tel = $parametri['ordtel'];
        $order->orario = $parametri['ordorario'];

        DB::store($order);
    }
}