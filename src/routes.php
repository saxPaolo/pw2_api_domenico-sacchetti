<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

/* $app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
}); */

$app->get('/', function($request, $response, $args) {
    return $response->getBody()->write('Homepage');
});

// Routes Prodotti
$app->get(
    '/products[/]', 
    \Controller\ProductsController::class.':getAllProducts'
);
$app->get(
    '/products/{id:[0-9]+}[/]', 
    \Controller\ProductsController::class.':getProduct'
);
$app->delete(
    '/products/{id:[0-9]+}[/]', 
    \Controller\ProductsController::class.':deleteProduct'
);
$app->post(
    '/products[/]',
    \Controller\ProductsController::class.':createProduct'
);
$app->put(
    '/product/{id:[0-9]+}[/]',
    \Controller\ProductsController::class.':updateProduct'
);

// Routes Ordini
$app->get(
    '/orders[/]',
    \Controller\OrdersController::class.':getAllOrders'
);
$app->get(
    '/orders/{id:[0-9]+}[/]', 
    \Controller\OrdersController::class.':getOrder'
);
$app->delete(
    '/orders/{id:[0-9]+}[/]', 
    \Controller\OrdersController::class.':deleteOrder'
);
$app->post(
    '/orders[/]',
    \Controller\OrdersController::class.':createOrder'
);
$app->put(
    '/orders/{id:[0-9]+}[/]',
    \Controller\OrdersController::class.':updateOrder'
);